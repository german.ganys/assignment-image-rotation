#include "bmp_util.h"
#include "transform.h"

#define ARGS_COUNT 3

#define ARGS_CHECK_MESSAGES_COUNT 2
enum args_check_status {
    ARGS_OK = 0,
    LACK_OF_ARGS,
};

#define FILE_MESSAGES_COUNT 3
enum file_validation_status {
    FILE_OK = 0,
    SOURCE_FILE_INVALID,
    DEST_FILE_INVALID,
};

char *file_error_messages[FILE_MESSAGES_COUNT] = {
        [SOURCE_FILE_INVALID] ="Source file invalid",
        [DEST_FILE_INVALID] ="Target file invalid",
};

char *args_check_messages[ARGS_CHECK_MESSAGES_COUNT] = {
        [LACK_OF_ARGS] = "Lack of args",
};

char *bmp_read_check_messages[READING_ERROR_MESSAGES_COUNT] = {
        [READ_INVALID_BITS] = "Invalid bits",
        [READ_INVALID_HEADER] = "Invalid header",
        [READ_INVALID_OFFSET] = "Invalid offset",
        [READ_INVALID_PADDING] = "Invalid padding",
        [READ_OUT_OF_MEMORY] = "Lack of memory",
};

char *bmp_write_check_messages[WRITING_ERROR_MESSAGES_COUNT] = {
        [WRITE_HEADER_ERROR] ="Headers write error",
        [WRITE_BODY_ERROR] = "Body write error",
};


static inline enum args_check_status validate_args(int argc) {
    if (argc < ARGS_COUNT) return LACK_OF_ARGS;
    if (argc > ARGS_COUNT) fprintf(stderr, "Notice: too many arguments");

    return ARGS_OK;
}

static inline enum file_validation_status prepare_files(
        const char *in_filename, const char *out_filename,
        FILE **file_in, FILE **file_out) {

    *file_in = fopen(in_filename, "rb");
    if (*file_in == NULL)
        return SOURCE_FILE_INVALID;

    *file_out = fopen(out_filename, "wb");
    if (*file_out == NULL)
        return DEST_FILE_INVALID;

    return FILE_OK;

}

int main(int argc, char **argv) {
    FILE *file_in, *file_out;

    enum args_check_status arg_stat = validate_args(argc);

    if (arg_stat != ARGS_OK) {
        fprintf(stderr, "%s", args_check_messages[arg_stat]);
        return 1;
    }

    char *in_filename = argv[1];
    char *out_filename = argv[2];

    enum file_validation_status f_status = prepare_files(in_filename, out_filename, &file_in, &file_out);

    if (f_status != FILE_OK) {
        fprintf(stderr, "Input file error: %s", file_error_messages[f_status]);
        return 1;
    }


    struct image img = {0};

    enum read_status r_status = from_bmp(file_in, &img);

    if (r_status != READ_OK) {
        fprintf(stderr, "Error while reading image from %s: %s\n",
                in_filename, bmp_read_check_messages[r_status]);
        if (r_status != READ_OUT_OF_MEMORY)
            image_free(&img);
        fclose(file_in);
        return 1;
    }
    fclose(file_in);

    struct image transformed;
    enum transform_stat e_status = image_cw_rotate90(img, &transformed);

    if (e_status != EDIT_OK) {
        fprintf(stderr, "Error while transformation: %s\n", bmp_read_check_messages[r_status]);

        if (e_status != EDIT_OUT_OF_MEMORY)
            image_free(&transformed);

        image_free(&img);
        return 1;
    }
    image_free(&img);

    enum write_stat w_stat = to_bmp(file_out, &transformed);
    if (w_stat != WRITE_OK) {
        fprintf(stderr, "Couldn't write image to %s: %s\n", out_filename, bmp_write_check_messages[w_stat]);

        image_free(&transformed);
        fclose(file_out);
        return 1;
    }

    image_free(&transformed);
    fclose(file_out);

    return 0;
}
