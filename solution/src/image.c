#include "image.h"
#include <malloc.h>

struct image image_create(uint32_t height, uint32_t width) {
    struct pixel **matrix = (struct pixel**) malloc(height * sizeof(struct pixel*));
    for (size_t i = 0; i < height; i++)
        matrix[i] = (struct pixel*) malloc(width * sizeof(struct pixel));

    return (struct image) {
            .height = height,
            .width = width,
            .pixel_matrix = matrix,
    };
}

void image_free(struct image *img) {
    if (img != NULL) {
        for (size_t i = 0; i < img->height; i++)
            free(img->pixel_matrix[i]);
        free(img->pixel_matrix);
    }
}

void set_pixel(struct image *img, uint32_t x, uint32_t y, struct pixel pixel) {
    if (img != NULL)
        img->pixel_matrix[y][x] = pixel;
}
