#include "transform.h"

enum transform_stat image_cw_rotate90(struct image const source, struct image *dest) {
    // Переставляем местами height и width, тк поворачиваем на 90.
    size_t new_w = source.height;
    size_t new_h = source.width;

    *dest = image_create(new_h, new_w);

    if (dest->pixel_matrix == NULL)
        return EDIT_OUT_OF_MEMORY;

    for (uint32_t y = 0; y < source.height; y++)
        for (uint32_t x = 0; x < source.width; x++)
            set_pixel(dest, source.height - y - 1, x,
                      source.pixel_matrix[y][x]);
    return EDIT_OK;
}
