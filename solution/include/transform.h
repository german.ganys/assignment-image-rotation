#ifndef ROTATE
#define ROTATE

#include "bmp_util.h"
#include "image.h"

enum transform_stat image_cw_rotate90(struct image source, struct image *dest);

#endif
