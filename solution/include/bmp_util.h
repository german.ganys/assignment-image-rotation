#ifndef BMP_SERIALIZATION
#define BMP_SERIALIZATION

#include <stdio.h>
#include "image.h"

#define READING_ERROR_MESSAGES_COUNT 6
#define EDITING_ERROR_MESSAGES_COUNT 2
#define WRITING_ERROR_MESSAGES_COUNT 3

enum read_status {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_OFFSET,
    READ_INVALID_PADDING,
    READ_OUT_OF_MEMORY
};

enum read_status from_bmp(FILE *in, struct image *dest);

enum transform_stat {
    EDIT_OK = 0,
    EDIT_OUT_OF_MEMORY
};

enum write_stat {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_BODY_ERROR
};

enum write_stat to_bmp(FILE *out, struct image const *src);
#endif
