#ifndef IMAGE
#define IMAGE

#include <stdint.h>

struct __attribute__((__packed__)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint32_t width, height;

    struct pixel **pixel_matrix;
};

void set_pixel(struct image *img,
               uint32_t x, uint32_t y,
               struct pixel pixel);

struct image image_create(uint32_t height, uint32_t width);

void image_free(struct image *img);

#endif
